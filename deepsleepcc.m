#import <spawn.h>
#import "deepsleepcc.h"

@implementation deepsleepcc

- (UIImage *)iconGlyph {
	return [UIImage imageNamed:@"ModuleIcon" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
}

- (UIColor *)selectedColor {
	return [UIColor blueColor];
}

- (BOOL)isSelected {
  return false;
}

- (void)setSelected:(BOOL)selected {
  pid_t pid;
  const char* args[] = {"mobiledeepsleep", NULL};
  posix_spawn(&pid, "/usr/bin/mobiledeepsleep", NULL, NULL, (char* const*)args, NULL);
}

@end
