include $(THEOS)/makefiles/common.mk

CURDIR := $(shell pwd)

export TARGET = iphone:clang:11.2:11.0
export ARCHS = arm64

BUNDLE_NAME = deepsleepcc
deepsleepcc_BUNDLE_EXTENSION = bundle
deepsleepcc_FILES = deepsleepcc.m
deepsleepcc_PRIVATE_FRAMEWORKS = ControlCenterUIKit
deepsleepcc_INSTALL_PATH = /Library/ControlCenter/Bundles/

after-stage::
	echo $(CURDIR)
	$(MAKE) -C deepsleep ios
	mkdir $(THEOS_STAGING_DIR)/Library/BawAppie
	mkdir $(THEOS_STAGING_DIR)/Library/BawAppie/deepsleepcc
	mv $(CURDIR)/deepsleep/deepsleep_arm64 $(THEOS_STAGING_DIR)/Library/BawAppie/deepsleepcc/deepsleep

include $(THEOS_MAKE_PATH)/bundle.mk

SUBPROJECTS += mobiledeepsleep

after-install::
	install.exec "killall -9 SpringBoard"

include $(THEOS_MAKE_PATH)/aggregate.mk
